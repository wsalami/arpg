﻿Shader "FullScreen/Pixelate"
{
    HLSLINCLUDE

    #pragma vertex Vert

    #pragma target 4.5
    #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/RenderPass/CustomPass/CustomPassCommon.hlsl"

    TEXTURE2D_X(_DepthTex);
    TEXTURE2D_X(_PixelTexture);
    TEXTURE2D_X(_DepthTexCam);

    float _PixelDensity;
    int _PosterizationCount;
    float _Power;
    float _OutlineSize;

    float SampleDepth(float2 uv)
    {
        return SAMPLE_TEXTURE2D_X_LOD(_DepthTex, s_point_clamp_sampler, uv,0).r;
    }

    float SampleDepthCam(float2 uv)
    {
        return SAMPLE_TEXTURE2D_X_LOD(_DepthTexCam, s_point_clamp_sampler, uv, 0).r;
    }

    float2 ClampUVs(float2 uv)
    {
        uv = clamp(uv, 0, _RTHandleScale - _ScreenSize.zw);
        return uv;
    }

    float2 sobel(float2 uv)
    {
        float outline = min(_PixelDensity, _OutlineSize);
        float2 delta = float2(outline / _ScreenSize.x, outline / _ScreenSize.y);

        float up = SampleDepth(ClampUVs(uv + ClampUVs(float2(0.0, 1.0) * delta)));
        float down = SampleDepth(ClampUVs(uv + ClampUVs(float2(0.0, -1.0) * delta)));
        float left = SampleDepth(ClampUVs(uv + ClampUVs(float2(1.0, 0.0) * delta)));
        float right = SampleDepth(ClampUVs(uv + ClampUVs(float2(-1.0, 0.0) * delta)));
        float centre = SampleDepth(uv);

        float depth = max(max(up, down), max(left, right));
        return float2(clamp(up - centre, 0, 1) + clamp(down - centre, 0, 1) + clamp(left - centre, 0, 1) + clamp(right - centre, 0, 1), depth);
    }


    half4 frag(Varyings input, out float depth : SV_Depth) : SV_Target
    {

        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
        float depthCam = LoadCameraDepth(input.positionCS.xy);
        PositionInputs posInput = GetPositionInput(input.positionCS.xy, _ScreenSize.zw, depthCam, UNITY_MATRIX_I_VP, UNITY_MATRIX_V);
        float2 uv = ClampUVs(posInput.positionNDC.xy * _RTHandleScale.xy);
        

        float2 sobelData = sobel(uv);
        float s = pow(abs(1 - saturate(sobelData.x)), _Power);
        float4 col = SAMPLE_TEXTURE2D_X_LOD(_PixelTexture, s_point_clamp_sampler, uv, 0);
        
        float x = ceil(SampleDepth(uv) - SampleDepthCam(uv));
        

        col.a = x;
        col = pow(abs(col), 0.4545);
        float3 c = RgbToHsv(col);
        c.z = round(c.z * _PosterizationCount) / _PosterizationCount;
        col = float4(HsvToRgb(c), col.a);
        col = pow(abs(col), 2.2);
        
        s = floor(s + 0.2);

        s = lerp(1.0, s, ceil(sobelData.y - SampleDepthCam(uv)));

        depth = lerp(sobelData.y, SampleDepth(uv), s);
        
        col.rgb *= s;
        col.a += 1 - s;

        return col;
    }

    ENDHLSL

    SubShader
    {
        Tags{ "RenderType" = "Opaque" }
        LOD 200

        Pass
        {
            Name "PixelatePassPLS"

            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite On

            HLSLPROGRAM
                #pragma fragment frag
            ENDHLSL
        }
    }
    Fallback "Diffuse"
}