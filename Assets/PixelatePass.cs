﻿using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Profiling;
using UnityEditor.Rendering.LookDev;
using System.Runtime.Remoting.Messaging;

class PixelatePass : CustomPass
{
    [Range(1, 15)]
    public float pixelDensity = 1;
    [Range(1, 15)]
    public float OutlineSize = 1;
    public float power = 15;
    public int postCount = 8;
    public LayerMask maskLayer;
    private Material pixMat;

    ShaderTagId[] shaderTags;

    // Trick to always include these shaders in build
    [SerializeField, HideInInspector]
    Shader pixelateShader;

    RTHandle pixelCol;
    RTHandle pixelDepth;
    RTHandle tmp;

    static class ShaderID
    {
        public static readonly int _PixelColor = Shader.PropertyToID("_PixelTexture");
        public static readonly int _CameraColorTexture = Shader.PropertyToID("_CameraColorTexture");
        public static readonly int _CamDepthTex = Shader.PropertyToID("_DepthTexCam");
        public static readonly int _DepthTex = Shader.PropertyToID("_DepthTex");
        public static readonly int _DepthTexFull = Shader.PropertyToID("_DepthTexFull");
        public static readonly int _PixelDensity = Shader.PropertyToID("_PixelDensity");
        public static readonly int _Power = Shader.PropertyToID("_Power");
        public static readonly int _Poster = Shader.PropertyToID("_PosterizationCount");
        public static readonly int _OutlineSize = Shader.PropertyToID("_OutlineSize");
    }

    // It can be used to configure render targets and their clear state. Also to create temporary render target textures.
    // When empty this render pass will render to the active camera render target.
    // You should never call CommandBuffer.SetRenderTarget. Instead call <c>ConfigureTarget</c> and <c>ConfigureClear</c>.
    // The render pipeline will ensure target setup and clearing happens in an performance manner.
    protected override void Setup(ScriptableRenderContext renderContext, CommandBuffer cmd)
    {
        if (pixelateShader == null)
            pixelateShader = Shader.Find("FullScreen/Pixelate");

        pixMat = CoreUtils.CreateEngineMaterial(pixelateShader);

        shaderTags = new ShaderTagId[4]
        {
            new ShaderTagId("Forward"),
            new ShaderTagId("ForwardOnly"),
            new ShaderTagId("SRPDefaultUnlit"),
            new ShaderTagId("FirstPass"),
        };
    }

    void AllocatePixelAndDepthBuffersIfNeeded()
    {
        var hdrpAsset = (GraphicsSettings.renderPipelineAsset as HDRenderPipelineAsset);
        var colorBufferFormat = hdrpAsset.currentPlatformRenderPipelineSettings.colorBufferFormat;
        // Allocate the buffers according to pixel density
        pixelCol = RTHandles.Alloc(
                Vector2.one / pixelDensity, TextureXR.slices, dimension: TextureXR.dimension,
                colorFormat: (GraphicsFormat)colorBufferFormat,
                filterMode: FilterMode.Point,
                wrapMode: TextureWrapMode.Clamp,
                useDynamicScale: true, name: "PixColorBuffer"
            );
        


        pixelDepth = RTHandles.Alloc(
                Vector2.one /pixelDensity, TextureXR.slices, dimension: TextureXR.dimension,
                depthBufferBits: DepthBits.Depth32,
                colorFormat: GraphicsFormat.R32_UInt,
                filterMode: FilterMode.Point,
                wrapMode: TextureWrapMode.Clamp,
                useDynamicScale: true, name: "PixDepthBuffer"
            );
    }
    protected override void Execute(ScriptableRenderContext renderContext, CommandBuffer cmd, HDCamera hdCamera, CullingResults cullingResult)
    {

        AllocatePixelAndDepthBuffersIfNeeded();

        if (pixMat != null)
        {
            RTHandle source;
            RTHandle depthSource;

            tmp = RTHandles.Alloc(
                Vector2.one , TextureXR.slices, dimension: TextureXR.dimension,
                depthBufferBits: DepthBits.Depth32,
                colorFormat: GraphicsFormat.R32_UInt,
                filterMode: FilterMode.Point,
                wrapMode: TextureWrapMode.Clamp,
                useDynamicScale: true, name: "PixDepthBuffer"
            );

            GetCameraBuffers(out source, out depthSource);

            // Render the objects in the layer on a 
            var result = new RendererListDesc(shaderTags, cullingResult, hdCamera.camera)
            {
                rendererConfiguration = PerObjectData.LightProbe | PerObjectData.LightProbeProxyVolume | PerObjectData.Lightmaps,
                renderQueueRange = RenderQueueRange.opaque,
                sortingCriteria = SortingCriteria.BackToFront,
                excludeObjectMotionVectors = false,
                layerMask = maskLayer,
                stateBlock = new RenderStateBlock(RenderStateMask.Depth) { depthState = new DepthState(true, CompareFunction.LessEqual) },
            };

            using (new ProfilingSample(cmd, "Draw Pixels & Depth", CustomSampler.Create("Draw Pixels & Depth")))
            {

                

                //We draw renderers of pixelated objects low res for depth + color and everything else high res for dep
                CoreUtils.SetRenderTarget(cmd, pixelCol, pixelDepth);
                HDUtils.DrawRendererList(renderContext, cmd, RendererList.Create(result));
                

            }
            using (new ProfilingSample(cmd, "Pixelate", CustomSampler.Create("Pixelate")))
            {

                GetCameraBuffers(out source, out depthSource);

                var compositingProperties = new MaterialPropertyBlock();

                compositingProperties.SetTexture(ShaderID._PixelColor, pixelCol);
                compositingProperties.SetTexture(ShaderID._DepthTex,pixelDepth);
                compositingProperties.SetTexture(ShaderID._CamDepthTex, depthSource);
                compositingProperties.SetInt(ShaderID._Poster, postCount);
                compositingProperties.SetFloat(ShaderID._PixelDensity, pixelDensity);
                compositingProperties.SetFloat(ShaderID._Power, power);
                compositingProperties.SetFloat(ShaderID._OutlineSize, OutlineSize);

                HDUtils.DrawFullScreen(cmd, pixMat, source, shaderPassId: 0, properties: compositingProperties);
                HDUtils.DrawFullScreen(cmd, pixMat, depthSource,shaderPassId: 0,properties:compositingProperties);

                SetCameraRenderTarget(cmd);
            }
        }
    }

    protected override void AggregateCullingParameters(ref ScriptableCullingParameters cullingParameters, HDCamera hdCamera) => cullingParameters.cullingMask = (uint)hdCamera.camera.cullingMask | (uint)maskLayer.value; 

    // release all resources
    protected override void Cleanup()
    {
        CoreUtils.Destroy(pixMat);
        pixelCol.Release();
        pixelDepth.Release();
        tmp.Release();
    }
}
